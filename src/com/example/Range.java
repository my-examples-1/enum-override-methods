package com.example;

public class Range {

  private String aggregationName;
  private String value;
  private boolean min;

  public void setAggregation(String aggregationName, boolean min, String value) {
    this.aggregationName = aggregationName;
    this.min = min;
    this.value = value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "Range{" +
        "aggregationName='" + aggregationName + '\'' +
        ", value='" + value + '\'' +
        ", min=" + min +
        '}';
  }
}
