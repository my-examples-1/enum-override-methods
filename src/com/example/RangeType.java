package com.example;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public enum RangeType {
  UO_UO( "UO", false) {
    @Override
    public void setValue(Range range, String value) {
      range.setAggregation(UO_UO.agrName, UO_UO.min, value);
    }
  },
  TIME_END("TIMEEND", false) {
    @Override
    public void setValue(Range range, String value) {
      range.setValue(value);
    }
  };

  private final String agrName;
  private final boolean min;

  public void setValue(Range range, String value) {
    throw new NotImplementedException();
  }

  RangeType(String agrName, boolean min) {
    this.agrName = agrName;
    this.min = min;
  }
}
