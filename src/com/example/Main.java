package com.example;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Main {

  public static void main(String[] args) {
    List<String> columns = Arrays.asList("UO_UO", "TIME_END");
    Range range = new Range();
    columns.forEach(column -> {
         try {
           RangeType.valueOf(column).setValue(range, UUID.randomUUID().toString());
         }catch (IllegalArgumentException ex){
           throw new RuntimeException("some message");
         }
        }
    );
    System.out.println(range);
  }
}
